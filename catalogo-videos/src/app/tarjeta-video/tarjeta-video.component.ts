import { Component, OnInit, HostBinding, Input } from '@angular/core';
import { FichaVideo } from '../models/ficha-video.model';

@Component({
  selector: 'app-tarjeta-video',
  templateUrl: './tarjeta-video.component.html',
  styleUrls: ['./tarjeta-video.component.css']
})
export class TarjetaVideoComponent implements OnInit {

  @HostBinding('attr.class')
  cssClass = 'col-md-2';

  @Input()
  tarjetaVideo: FichaVideo;

  constructor() { }

  ngOnInit() {
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaTarjetasVideosComponent } from './lista-tarjetas-videos.component';

describe('ListaTarjetasVideosComponent', () => {
  let component: ListaTarjetasVideosComponent;
  let fixture: ComponentFixture<ListaTarjetasVideosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaTarjetasVideosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaTarjetasVideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

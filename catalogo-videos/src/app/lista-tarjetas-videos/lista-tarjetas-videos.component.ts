import { Component, OnInit } from '@angular/core';
import { FichaVideo } from '../models/ficha-video.model';

@Component({
  selector: 'app-lista-tarjetas-videos',
  templateUrl: './lista-tarjetas-videos.component.html',
  styleUrls: ['./lista-tarjetas-videos.component.css']
})
export class ListaTarjetasVideosComponent implements OnInit {

  listaFichasVideos: FichaVideo[];

  constructor() {
    this.listaFichasVideos = []; 
    this.listaFichasVideos.push(new FichaVideo("Man Of Steal", "http://henrycavill.org/images/Films/2013-Man-of-Steel/posters/3-Walmart-Superman-a.jpg"));
    this.listaFichasVideos.push(new FichaVideo("Glass", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/svIDTNUoajS8dLEo7EosxvyAsgJ.jpg"));
    this.listaFichasVideos.push(new FichaVideo("Mortal Enignes", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/uXJVpPXxZO4L8Rz3IG1Y8XvZJcg.jpg"));
    /*this.listaFichasVideos.push(new FichaVideo("Dragon Ball Super", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/f03YksE4NggUjG75toz4H1YAGRf.jpg"));
    this.listaFichasVideos.push(new FichaVideo("Aquaman", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/5Kg76ldv7VxeX9YlcQXiowHgdX6.jpg"));
    this.listaFichasVideos.push(new FichaVideo("A star is born", "https://image.tmdb.org/t/p/w600_and_h900_bestv2/wrFpXMNBRj2PBiN4Z5kix51XaIZ.jpg"));
    */
  }

  ngOnInit() {
  }

  guardar(v:FichaVideo) {
    console.log(v);
    this.listaFichasVideos.push(v);
  }
}


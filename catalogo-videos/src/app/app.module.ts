import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TarjetaVideoComponent } from './tarjeta-video/tarjeta-video.component';
import { ListaTarjetasVideosComponent } from './lista-tarjetas-videos/lista-tarjetas-videos.component';
import { NewFichaVideoComponent } from './new-ficha-video/new-ficha-video.component';

@NgModule({
  declarations: [
    AppComponent,
    TarjetaVideoComponent,
    ListaTarjetasVideosComponent,
    NewFichaVideoComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

export class FichaVideo {
    nombre:string;
    imageUrl:string;

    constructor(n:string, u:string) {
        this.nombre = n;
        this.imageUrl = u;
    }
}
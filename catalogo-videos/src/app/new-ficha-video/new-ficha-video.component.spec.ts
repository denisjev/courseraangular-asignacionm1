import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewFichaVideoComponent } from './new-ficha-video.component';

describe('NewFichaVideoComponent', () => {
  let component: NewFichaVideoComponent;
  let fixture: ComponentFixture<NewFichaVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewFichaVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewFichaVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

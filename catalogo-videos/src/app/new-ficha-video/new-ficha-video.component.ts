import { Component, OnInit, HostBinding, Input, Output, EventEmitter } from '@angular/core';
import { FichaVideo } from '../models/ficha-video.model';

@Component({
  selector: 'app-new-ficha-video',
  templateUrl: './new-ficha-video.component.html',
  styleUrls: ['./new-ficha-video.component.css']
})
export class NewFichaVideoComponent implements OnInit {

  @HostBinding('attr.class') cssClass = 'col-md-6	offset-md-3';
  @Input() nombre: string;
  @Input() url: string;
  @Output() clicked: EventEmitter<FichaVideo>;

  constructor() { 
    this.clicked = new EventEmitter<FichaVideo>();
  }

  ngOnInit() {
  }

  guardar(n:string, u:string):boolean {
    this.clicked.emit(new FichaVideo(n, u));
    return false;
  }
}
